const express = require('express');
const router = express.Router();

const homeController = 
        require('../controllers/home.controller');

router.get('/', homeController.index);

const contactoController = 
        require('../controllers/contacto.controller');

router.get('/contactenos', contactoController.contactenos);

const miyviController = 
        require('../controllers/miyvi.controller');

router.get('/mision-vision', miyviController.misionyvision);
module.exports = router;